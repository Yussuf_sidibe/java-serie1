package org.YussufSidibe.exo2;
import java.math.*;
public class Factorielle {
	
	public int factorielle(int nbrEntier){
		int result=1;
		for(int i=1;i<=nbrEntier;i++)
			result=result*i;
		return result;
	}
	public double factorielle(double nbrDecimal){
		double result=1.0;
		for(double i=1;i<=nbrDecimal;i++)
			result=result*i;
		return result;
	}
	public BigInteger factorielle(BigInteger nbrDecimal){
		BigInteger result=BigInteger.ONE;
		for(BigInteger  i=BigInteger.ONE;i.compareTo(nbrDecimal)<=0;i=i.add(BigInteger.ONE))
			result=result.multiply(i);
		return result;
	}

}
