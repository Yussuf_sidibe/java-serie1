package org.YussufSidibe.exo2;
import java.math.*;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int nbrE=30;
		double nbrD=30.0;
		BigInteger nbrB=new BigInteger("30");
		Factorielle fact= new Factorielle();
		System.out.println("Factorielle de "+nbrE+"(Entier) ="+fact.factorielle(nbrE));
		System.out.println("Factorielle de "+nbrD+"(Decimal) ="+fact.factorielle(nbrD));
		System.out.println("Factorielle de "+nbrB+"(BigInteger) ="+fact.factorielle(nbrB));
	}

}
