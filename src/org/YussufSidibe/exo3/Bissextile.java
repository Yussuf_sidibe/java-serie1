package org.YussufSidibe.exo3;

public class Bissextile {

	public boolean bissextile(int an){
		
		if(an%100!=0 && an%4==0 || an%400==0 ){
			return true;
		}
		else return false;	
	}
}
