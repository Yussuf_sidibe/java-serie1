package org.YussufSidibe.exo4;

public class Palindrome {
	
	public boolean palindrome(String chaine){
		boolean test=true;
		int j;
		String ch=new String();
		ch=chaine.toUpperCase();
		j=ch.length()-1;
		for(int i=0;i<j && test;i++){
			if(ch.charAt(i)==' ')
				i++;
			if (ch.charAt(j)==' ')
				j--;
			if (ch.charAt(i)==ch.charAt(j))
				j--;
			else
				test=false;
		}
		return test;
	}

}
